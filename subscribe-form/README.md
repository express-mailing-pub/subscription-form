# Côté serveur 

Ce guide vous explique comment connecter facilement votre formulaire au service Express Mailing afin d'envoyer et gérer automatiquement les informations de vos contacts.

## Point d'entrée de l'API

Le point d'entrée pour poster les données se fera à cette adresse : `https://my.express-mailing.com/landing/subscribe-json`

## Encodage du formulaire
Vous pouvez choisir entre deux formats pour envoyer les données du formulaire : 

 - **`application/x-www-form-urlencoded`** : format classique utilisé généralement.
 - **`multipart/form-data`** : utile notamment si vous envoyez des fichiers ou des données volumineuses.
## Valeurs possibles pour les champs

### Champs de données
Tous ces champs sont optionnels sauf l'email.

| Champ | Type | Observations | Validation | Optionnel |
| - | - | - | - | - |
| email | string | Email du contact| 200 caractères maximum | Non |
| civ | enum `["", "Monsieur", "Madame", "Mademoiselle", "Société", "M.", "Mme.", "Mlle.", "Sci.", "M", "MME", "MLLE", "SCI"]` | Civilité du contact | | Oui |
|name| string | Nom du contact | 150 caractères maximum | Oui |
|prenom | string | Prénom du contact | 150 caractères maximum | Oui |
|societe | string | Société du contact | 200 caractères maximum | Oui |
|adresse1 | string | Première ligne de l'adresse du contact | 200 caractères maximum | Oui |
|adresse2 | string | Deuxième ligne de l'adresse du contact | 200 caractères maximum | Oui |
|cp | string | Code postal du contact | 10 caractères maximum | Oui |
|ville | string | Ville du contact | 200 caractères maximum | Oui |
|region | string | Région du contact | 100 caractères maximum | Oui |
|pays | string | Pays du contact | 100 caractères maximum | Oui |
|tel | string | Numéro de téléphone du contact | 20 caractères maximum | Oui |
|fax | string | Numéro de fax du contact |20 caractères maximum | Oui |
|code | string | Code client du contact | 20 caractères maximum | Oui |
|fonction | string | Profession du contact | 100 caractères maximum | Oui |
|libre1 | string | Champ libre 1 du contact | 255 caractères maximum | Oui |
|libre2 | string | Champ libre 2 du contact | 255 caractères maximum | Oui |
|libre3 | string | Champ libre 3 du contact | 255 caractères maximum | Oui |
|libre4 | string | Champ libre 4 du contact | 255 caractères maximum | Oui |
|libre5 | string | Champ libre 5 du contact | 255 caractères maximum | Oui |
|birthday | Date object | Date d'anniversaire du contact | jour : min 1, max 31 ; mois : min 1, max 12 | Oui

### Champs de contrôles

Nom | Type | Observations | Validation | Optionnel
| - | - | - | - | - |
|liste | string ou string array | Code(s) de la/les liste(s) où doit être inséré le contact. [Voir ici](#récupérer-le-code-liste) | 8 caractères | Non |
|messageSuccess | string | Message qui apparaît en cas de **succès** de l'insertion du contact. | 500 caractères maximum | Oui |
|messageFail | string | Message qui apparaît en cas d'**échec** de l'insertion du contact. | 500 caractères maximum | Oui |
|redirectionSuccess | string | URL de redirection en cas de **succès** de l'insertion du contact. | 500 caractères maximum | Oui |
|redirectionFail | string | URL de redirection en cas d'**échec** de l'insertion du contact. | 500 caractères maximum | Oui |
|honeypot | any | Champ invisible pour protéger le formulaire des bots. | / | Oui |



## Code liste et abonnement multi-listes

### Comment récupérer les codes des listes ?

Connectez-vous à votre espace sur [my.express-mailing.com](https://my.express-mailing.com/), puis :
1. Allez dans l'onglet **Mes abonnés**. 
2. Cliquez sur la liste souhaitée.
3. Rendez-vous dans **Paramètres**.
4. Dans le champ **« CODE ROUGE » de la liste**, copiez/collez le code qui s'affiche.

### Inscrire un contact à plusieurs listes à la fois

Vous pouvez inscrire automatiquement vos contacts à plusieurs listes simultanément en indiquant simplement plusieurs codes séparés par des virgules dans le champ `liste`.

Par exemple :

```liste: ["1234", "56747878"]```

Le contact sera automatiquement ajouté à toutes les listes indiquées.