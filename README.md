# Formulaire d'inscription opt-in



## Intégration du composant sur votre site

Pour intégrer le formulaire sur votre site : 
1. Copiez/collez le code HTML généré par notre outil.
2. Copiez/collez les balises script suivantes dans votre HTML :

```
<script async defer src="https://my.express-mailing.com/landing-elements/elements.js"></script>
```

## Personnaliser votre formulaire

Pour accéder au récapitulatif du noms des champs, des propriétés et des variables CSS, cliquez sur le lien : 
- [Accéder au Read Me](https://gitlab.com/express-mailing-pub/subscription-form/-/tree/main/embedable-component)

## Configuration côté serveur

Pour configurer votre formulaire d'inscription côté serveur avec l'accès à l'adresse API, les formats de données acceptés ainsi que la gestion de l'abonnement à une ou plusieurs listes. Cela vous assurera une intégration rapide et fiable avec la plateforme My Express Mailing.
- [Accéder au Read Me](https://gitlab.com/express-mailing-pub/subscription-form/-/tree/main/subscribe-form)

## Besoin d'aide ?

Si vous rencontrez un problème quelconque, contactez-nous à l'adresse [support@express-mailing.com](mailto:support@express-mailing.com) ✉️