# Côté client 
## Les champs

### Noms des champs
Chaque champ correspond à une donnée utilisateur à collecter via le formulaire.

|Nom  | Type |
|--|--|
|liste |
| civ |*string*  |
| name |*string*  |
| firstname |*string*  |
| address1 |*string*  |
| address2 |*string*  |
| cp |*string*  |
| city |*string*  |
| region |*string*  |
| country |*string*  |
| code |*string*  |
| company |*string*  |
| profession |*string*  |
| free1 |*string*  |
| free2 |*string*  |
| free3 |*string*  |
| free4 |*string*  |
| free5 |*string*  |
| phone |*string*  |
| fax |*string*  |
| birthday |*string*  |
| email |*string*  |



### Propriétés des champs
Ces propriétés servent à configurer visuellement et fonctionnellement chaque champ.

|Nom | Type | Commentaire | Attribut HTML | Exemple champ email | Optionnel
| - | - | - | - | - | - |
| label | string | Nom du label |[[champ]](#noms-des-champs)-label | `email-label` | Oui
| pattern | string | Expression régulière pour déterminer les valeurs acceptées | [[champ]](#noms-des-champs)-pattern | `email-pattern` | Oui
| placeholder | string | Valeur s'affichant dans l'input avant qu'une valeur n'y soit entrée | [[champ]](#noms-des-champs)-placeholder | `email-placeholder` | Oui
| visibility | "none", "required" ou "optional" | Détermine si le champ s'affiche ("none"), est requis ("required") ou est optionnel ("optional") | [[champ]](#noms-des-champs)-field | `email-field` | Non
| inputClass | string | Classes CSS relatives à l'input | [[champ]](#noms-des-champs)-input-class | `email-input-class` | Oui
| labelClass | string | Classes CSS relatives au label | [[champ]](#noms-des-champs)-label-class | `email-label-class` | Oui

Exemple avec juste les attributs HTML des champs.

## Balise HTML em-subscribe-form

La balise `<em-subscribe-form />` permet l'intégration facile et rapide du formulaire sur votre site web, avec un paramétrage simple via les attributs et les classes CSS.

## Variables CSS

Rendez-vous sur [notre plateforme Express Mailing](https://my.express-mailing.com/) puis **Mes abonnés > Cliquer sur la liste concernée > Aller dans l'onglet "Opt in"**. 

Vous trouverez à droite un panneau de configuration vous permettant de personnaliser votre formulaire.

Si vous souhaitez personnaliser votre formulaire sans utiliser le générateur (ou en modifiant le code déjà généré), vous trouverez ci-dessous les variables CSS permettant de personnaliser l'apparence complète du formulaire sans modifier le code HTML interne. Voici les catégories principales :

### Variables du formulaire
Personnalisation du cadre global du formulaire (fond, bordures, espacements).

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-form-bg-color` | background-color | Couleur de fond du formulaire | `<em-subscribe-form style="--em-form-bg-color: #FFFFFF;" />`
| `--em-form-brd-color` | border-color | Couleur de bordure du formulaire | `<em-subscribe-form style="--em-form-brd-color: #FFFFFF;" />`
| `--em-form-brd-radius` | border-radius | Arrondis de bordure du formulaire | `<em-subscribe-form style="--em-form-brd-radius: 1em;" />`
| `--em-form-brd-width` | border-width | Taille de bordure du formulaire | `<em-subscribe-form style="--em-form-brd-width: 1px;" />`
| `--em-form-brd-style` | border-style | Style de bordure du formulaire | `<em-subscribe-form style="--em-form-brd-style: solid;" />`
| `--em-form-gap-x` | column-gap | Taille des espaces entre les colonnes du formulaire | `<em-subscribe-form style="--em-form-gap-x: 10px;" />`
| `--em-form-gap-y` | row-gap | Taille des espaces entre les lignes du formulaire | `<em-subscribe-form style="--em-form-gap-y: 10px;" />`
| `--em-form-padding-x` | padding-left padding-right | Padding à gauche et à droite du formulaire. | `<em-subscribe-form style="--em-form-padding-x: 20px;" />`
| `--em-form-padding-y` | padding-top padding-bottom | Padding en haut et en bas du formulaire. | `<em-subscribe-form style="--em-form-padding-y: 20px;" />`

### Variables du titre du formulaire
Couleur, taille et graisse du titre.

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-title-text-color` | color | Couleur du texte du titre de formulaire. | `<em-subscribe-form style="--em-title-text-color: #FFFFFF;" />`
| `--em-title-text-size` | font-size | Taille de police du titre de formulaire. | `<em-subscribe-form style="--em-title-text-size: 24px;" />`
| `--em-title-text-weight` | font-weight | Épaisseur de police du titre de formulaire. | `<em-subscribe-form style="--em-title-text-weight: bold;" />`

### Variables du label
Couleur, taille et poids des labels.

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-label-text-color` | color | Couleur du texte du label. | `<em-subscribe-form style="--em-label-text-color: #FFFFFF;" />`
| `--em-label-text-size` | font-size | Taille de police du label. | `<em-subscribe-form style="--em-label-text-size: 16px;" />`
| `--em-label-text-weight` | font-weight | Épaisseur de police du label. | `<em-subscribe-form style="--em-label-text-weight: bold;" />`

### Variables de l'input
Apparence des champs de saisie (couleurs, bordures, padding).

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-input-bg-color` | background-color | Couleur de fond de l'input. | `<em-subscribe-form style="--em-input-bg-color: #FFFFFF;" />`
| `--em-input-text-color` | color | Couleur du texte de l'input. | `<em-subscribe-form style="--em-input-text-color: #FFFFFF;" />`
| `--em-input-brd-color` | border-color | Couleur de bordure de l'input. | `<em-subscribe-form style="--em-input-brd-color: #FFFFFF;" />`
| `--em-input-brd-radius` | border-radius | Arrondis de bordure de l'input. | `<em-subscribe-form style="--em-input-brd-radius: 1em;" />`
| `--em-input-brd-width` | border-width | Taille de bordure de l'input. | `<em-subscribe-form style="--em-input-brd-width: 1px;" />`
| `--em-input-brd-style` | border-style | Style de bordure de l'input. | `<em-subscribe-form style="--em-input-brd-style: solid;" />`
| `--em-input-padding-x` | padding-left padding-right | Padding à gauche et à droite de l'input. | `<em-subscribe-form style="--em-input-padding-x: .75em;" />`
| `--em-input-padding-y` | padding-top padding-bottom | Padding en haut et en bas de l'input. | `<em-subscribe-form style="--em-input-padding-y: .25em;" />`
| `--em-input-text-size` | font-size | Taille du texte de l'input. | `<em-subscribe-form style="--em-input-text-size: 16px;" />`
| `--em-input-text-weight` | font-weight | Épaisseur du texte de l'input. | `<em-subscribe-form style="--em-input-text-weight: normal;" />`
| `--em-input-focus-color` | :focus-within outline-color | Couleur de bordure de l'input en focus. | `<em-subscribe-form style="--em-input-focus-color: yellow;" />`
| `--em-input-focus-width` | :focus-within outline-width | Taille de bordure de l'input en focus. | `<em-subscribe-form style="--em-input-focus-width: 1px;" />`
| `--em-input-focus-style` | :focus-within outline-style | Style de bordure de l'input en focus. | `<em-subscribe-form style="--em-input-focus-style: solid;" />`

### Variables du placeholder
Couleur et poids du texte d'indication dans les inputs.

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-plch-text-color` | color | Couleur du texte du placeholder. | `<em-subscribe-form style="--em-plch-text-color: #FFFFFF;" />`
| `--em-plch-text-weight` | font-weight | Épaisseur de police du placeholder. | `<em-subscribe-form style="--em-plch-text-weight: normal;" />`

### Variables du bouton
Style du bouton d'envoi du formulaire.

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-submit-bg-color` | background-color | Couleur de fond du bouton d'envoi. | `<em-subscribe-form style="--em-submit-bg-color: #FFFFFF;" />`
| `--em-submit-hover-bg-color` | background-color (:hover) | Couleur de fond du bouton d'envoi au survol. | `<em-subscribe-form style="--em-submit-hover-bg-color: #FFFFFF;" />`
| `--em-submit-selected-bg-color` | background-color (selected) | Couleur de fond du bouton d'envoi sélectionné. | `<em-subscribe-form style="--em-submit-selected-bg-color: #FFFFFF;" />`
| `--em-submit-disabled-bg-color` | background-color (:disabled) | Couleur de fond du bouton d'envoi désactivé. | `<em-subscribe-form style="--em-submit-disabled-bg-color: #FFFFFF;" />`
| `--em-submit-fg-color` | color | Couleur du texte du bouton. | `<em-subscribe-form style="--em-submit-fg-color: #FFFFFF;" />`
| `--em-submit-hover-fg-color` | color (:hover) | Couleur du texte du bouton au survol. | `<em-subscribe-form style="--em-submit-hover-fg-color: #FFFFFF;" />`
| `--em-submit-disabled-fg-color` | color (:disabled) | Couleur du texte du bouton désactivé. | `<em-subscribe-form style="--em-submit-disabled-fg-color: #FFFFFF;" />`
| `--em-submit-brd-color` | border-color | Couleur de bordure du bouton. | `<em-subscribe-form style="--em-submit-brd-color: #FFFFFF;" />`
| `--em-submit-brd-radius` | border-radius | Arrondis de bordure du bouton. | `<em-subscribe-form style="--em-submit-brd-radius: 1em;" />`
| `--em-submit-brd-width` | border-width | Taille de bordure du bouton. | `<em-subscribe-form style="--em-submit-brd-width: 1px;" />`
| `--em-submit-brd-style` | border-style | Style de bordure du bouton. | `<em-subscribe-form style="--em-submit-brd-style: solid;" />`
| `--em-submit-padding-x` | padding-left padding-right | Padding à gauche et à droite du bouton. | `<em-subscribe-form style="--em-submit-padding-x: .75em;" />`
| `--em-submit-padding-y` | padding-top padding-bottom | Padding en haut et en bas du bouton. | `<em-subscribe-form style="--em-submit-padding-y: .25em;" />`
| `--em-submit-text-size` | font-size | Taille du texte du bouton. | `<em-subscribe-form style="--em-submit-text-size: 22px;" />`
| `--em-submit-text-weight` | font-weight | Épaisseur du texte du bouton. | `<em-subscribe-form style="--em-submit-text-weight: bold;" />`
| `--em-submit-text-weight` | font-weight | Épaisseur du texte du bouton. | `<em-subscribe-form style="--em-submit-text-weight: bold;" />`

### Variables du message success
Personnalisation du message de succès après soumission.

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-success-message-text-color` | color | Couleur du texte du message de succès du formulaire. | `<em-subscribe-form style="--em-success-message-text-color: #FFFFFF;" />`
| `--em-success-message-text-size` | font-size | Taille de police du message de succès du formulaire. | `<em-subscribe-form style="--em-success-message-text-size: 14px;" />`
| `--em-success-message-text-weight` | font-weight | Épaisseur de police du message de succès du formulaire. | `<em-subscribe-form style="--em-success-message-text-weight: normal;" />`

### Variables du message fail
Personnalisation du message d'erreur après soumission.

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-fail-message-text-color` | color | Couleur du texte du message d'erreur du formulaire. | `<em-subscribe-form style="--em-fail-message-text-color: #FFFFFF;" />`
| `--em-fail-message-text-size` | font-size | Taille de police du message d'erreur du formulaire. | `<em-subscribe-form style="--em-fail-message-text-size: 14px;" />`
| `--em-fail-message-text-weight` | font-weight | Épaisseur de police du message d'erreur du formulaire. | `<em-subscribe-form style="--em-fail-message-text-weight: normal;" />`

### Variables du bouton opt-in
Apparence du bouton de consentement utilisateur.

| Nom | Propriété CSS | Observations | Exemple
| - | - | - | - |
| `--em-optin-text-color` | color | Couleur du texte du message du bouton opt-in. | `<em-subscribe-form style="--em-fail-message-text-color: #FFFFFF;" />`
| `--em-optin-checked-color` | color | Couleur de validation du bouton opt-in. | `<em-subscribe-form style="--em-optin-checked-color: green;" />`
| `--em-optin-unchecked-color` | color | Couleur d'invalidation du bouton opt-in. | `<em-subscribe-form style="--em-optin-unchecked-color: red;" />`
| `--em-optin-text-size` | font-size | Taille de police du bouton opt-in. | `<em-subscribe-form style="--em-optin-text-size: 16px;" />`

## Utiliser un CSS personnalisé

### Pour le formulaire

Si vous souhaitez styliser le formulaire dans son ensemble, il faut rajouter l'attribut `form-class` dans la balise HTML `<em-subscribe-form />`. Vous y renseignerez vos classes CSS personnalisées.

Exemple : 

`<em-subscribe-form form-class="flex gap-2 my-5" />`

### Pour le label

Si vous souhaitez styliser le label, il faut rajouter l'attribut `label-class` précédé du nom du champ dans la balise HTML `<em-subscribe-form />` (voir les exemples du tableau [Propriété des champs](#propriétés-des-champs)). Vous y renseignerez vos classes CSS personnalisées.

Exemple : 

`<em-subscribe-form form-class="flex gap-2 my-5" name-label-class="bold-text mx-2" />`

### Pour l'input

Si vous souhaitez styliser l'input, il faut rajouter l'attribut `input-class` précédé du nom du champ dans la balise HTML `<em-subscribe-form />` (voir les exemples du tableau [Propriété des champs](#propriétés-des-champs)). Vous y renseignerez vos classes CSS personnalisées.

Exemple : 

`<em-subscribe-form form-class="flex gap-2 my-5" name-label-class="bold-text mx-2" name-input-class="rounded py-2 solid-border" />`